package cql.ecci.ucr.ac.miserviciossensores;

public class ResultadoGeoIPService {

    private String NombrePais;

    public String getNombrePais() {
        return NombrePais;
    }
    public void setNombrePais(String nombrePais) {
        NombrePais = nombrePais;
    }
    @Override
    public String toString() {
        return "GeoIPService [CountryName=" + NombrePais +  "]";
    }

}
